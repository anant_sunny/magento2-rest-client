var util = require('util');
var queryString = require('query-string');


module.exports = function (restClient) {
    var module = {};

    // inventory stock
    module.inventoryStock = function () {
        return restClient.get(`/products`);
    }


    return module;
}