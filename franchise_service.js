var util = require('util');
var queryString = require('query-string');

module.exports = function (restClient) {
    var module = {};
    // var token = `rqzr9u414knf5bi5yjwx0lfw7h1m1u9l`;
    
    module.orders = function () {   
        // var email = "anant_sunny@yahoo.com";
        // var from1 = encodeURIComponent(`${from}`);
        // var to1 = encodeURIComponent(`${to}`);
        // console.log(from,to);
        
	console.log('inside orders');

        //  var query2 = 'searchCriteria[filterGroups][0][filters][0][field]=created_at&searchCriteria[filterGroups][0][filters][0][value]='+from1+'&searchCriteria[filterGroups][0][filters][0][condition_type]=from&searchCriteria[filterGroups][1][filters][1][field]=created_at&searchCriteria[filterGroups][1][filters][1][value]='+to1+'&searchCriteria[filterGroups][1][filters][1][condition_type]=to';

        // var query1 = '&searchCriteria[filterGroups][3][filters][3][field]=status&searchCriteria[filterGroups][3][filters][3][value]='+status;
        var emailid = encodeURIComponent('anant@cashlu.com');
        var query = 'searchCriteria[filterGroups][0][filters][0][field]=customer_email&searchCriteria[filterGroups][0][filters][0][value]='+emailid;

        var endpointUrl = util.format('/orders?%s', query);
        
        return restClient.get(endpointUrl);
    };

    module.pincodewiseorder = function (postcode) {
     
        var query = 'searchCriteria[filterGroups][0][filters][0][field]=postcode&searchCriteria[filterGroups][0][filters][0][value]='+postcode;

        var endpointUrl = util.format('/orders?%s', query);
        return restClient.get(endpointUrl);
    };

    module.franchiseordersdetail = function(order_ids, status){

        var query = 'searchCriteria[filterGroups][0][filters][0][field]=status'+
                    '&searchCriteria[filterGroups][0][filters][0][value]='+status +
                    '&searchCriteria[filterGroups][0][filters][0][condition_type]=in';

        var query1 =  '&searchCriteria[filterGroups][1][filters][0][field]=entity_id'+
                    '&searchCriteria[filterGroups][1][filters][0][value]='+order_ids+
                    '&searchCriteria[filterGroups][1][filters][0][condition_type]=in';

        var actual_query = query + query1;            
        var endpointUrl = util.format('/orders?%s', actual_query);
        return restClient.get(endpointUrl);
    }

      
   
    return module;
}
