var util = require('util');
var queryString = require('query-string');

module.exports = function (restClient) {
    var module = {};

    module.commentupdate = function (customerToken, cartId, body) {
        console.log("body", body);
        console.log("token", customerToken);
        // console.log("orderId", orderId);
        if (customerToken && isNumeric(cartId)) {
            // return restClient.delete('/carts/mine/coupons', customerToken);
            return restClient.put(`/carts/mine/set-order-comment`, body);
        } else 
        {
            return restClient.put(`/guest-carts/${cartId}/set-order-comment`, body);
        }

        // return restClient.put(`/guest-carts/${cartId}/set-order-comment`, body);
    }


    module.commentupdatelist = function (customerToken, cartId, body) {
        console.log("body", body);
        console.log("token", customerToken);
        // console.log("orderId", orderId);
        if (customerToken != '') {
            // return restClient.delete('/carts/mine/coupons', customerToken);
            return restClient.put(`/carts/mine/set-order-custom-fields`, body, customerToken);
        } else 
        {
            return restClient.put(`/guest-carts/${cartId}/set-order-custom-field`, body);
        }

        // return restClient.put(`/guest-carts/${cartId}/set-order-comment`, body);
    }




    return module;
}