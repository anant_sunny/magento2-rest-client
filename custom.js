var util = require('util');
var queryString = require('query-string');

module.exports = function (restClient) {
    var module = {};
    // var token = `rqzr9u414knf5bi5yjwx0lfw7h1m1u9l`;

    module.singleorders = function (id) {
        // console.log();

        return restClient.get(`/orders/${id}`)
    }


    module.orders = function (from, to, status) {
        var email = "anant_sunny@yahoo.com";
        var from1 = encodeURIComponent(`${from}`);
        var to1 = encodeURIComponent(`${to}`);
        // console.log(from,to);



        var query2 = 'searchCriteria[filterGroups][0][filters][0][field]=created_at&searchCriteria[filterGroups][0][filters][0][value]=' + from1 + '&searchCriteria[filterGroups][0][filters][0][condition_type]=from&searchCriteria[filterGroups][1][filters][1][field]=created_at&searchCriteria[filterGroups][1][filters][1][value]=' + to1 + '&searchCriteria[filterGroups][1][filters][1][condition_type]=to';



        var query1 = '&searchCriteria[filterGroups][3][filters][3][field]=status&searchCriteria[filterGroups][3][filters][3][value]=' + status +
            '&searchCriteria[filterGroups][3][filters][3][condition_type]=in'

        var query = query2 + query1;

        var endpointUrl = util.format('/orders?%s', query);
        return restClient.get(endpointUrl);
    }


    module.getOrdersByDate = function (from, to) {
        var from1 = encodeURIComponent(`${from}`);
        var to1 = encodeURIComponent(`${to}`);
        var query2 = 'searchCriteria[filterGroups][0][filters][0][field]=created_at&searchCriteria[filterGroups][0][filters][0][value]=' + from1 + '&searchCriteria[filterGroups][0][filters][0][condition_type]=from&searchCriteria[filterGroups][1][filters][1][field]=created_at&searchCriteria[filterGroups][1][filters][1][value]=' + to1 + '&searchCriteria[filterGroups][1][filters][1][condition_type]=to';
        var query1 = '&searchCriteria[pageSize]=1000'
        var query = query2 + query1;
        var endpointUrl = util.format('/orders?%s', query);
        return restClient.get(endpointUrl);
    };


    module.ordersUsingOrderIdArray = function (order_ids) {
        var query1 = 'searchCriteria[filterGroups][1][filters][0][field]=entity_id' +
            '&searchCriteria[filterGroups][1][filters][0][value]=' + order_ids +
            '&searchCriteria[filterGroups][1][filters][0][condition_type]=in';

        var endpointUrl = util.format('/orders?%s', query1);
        return restClient.get(endpointUrl);
    }




    module.statuswiseorders = function (status) {

        var query = 'searchCriteria[filterGroups][0][filters][0][field]=status&searchCriteria[filterGroups][0][filters][0][value]=' + status + '&searchCriteria[filterGroups][0][filters][0][condition_type]=in';

        var endpointUrl = util.format('/orders?%s', query);
        return restClient.get(endpointUrl);
    };

    module.statuswiseordersPaging = function (status, page_number, len) {
        var query = 'searchCriteria[filterGroups][0][filters][0][field]=status&searchCriteria[filterGroups][0][filters][0][value]=' + status + '&searchCriteria[filterGroups][0][filters][0][condition_type]=in';
        var query2 = "&searchCriteria[currentPage]=" + page_number + "&searchCriteria[pageSize]=" + len;
        var actual_query = query + query2;

        var endpointUrl = util.format('/orders?%s', actual_query);
        return restClient.get(endpointUrl);
    };
    module.statuswiseordersPagingDesc = function (status, page_number, len) {
        var query = 'searchCriteria[filterGroups][0][filters][0][field]=status&searchCriteria[filterGroups][0][filters][0][value]=' + status + '&searchCriteria[filterGroups][0][filters][0][condition_type]=in';
        var query3 = '&searchCriteria[sortOrders][0][field]=entity_id&searchCriteria[sortOrders][1][direction]=DESC';
        var query2 = "&searchCriteria[currentPage]=" + page_number + "&searchCriteria[pageSize]=" + len;
        var actual_query = query + query3 + query2;
        var endpointUrl = util.format('/orders?%s', actual_query);
        return restClient.get(endpointUrl);
    };

        module.statusDatewiseordersPaging = function(from, to, status,page_number, len){
        var from1 = encodeURIComponent(`${from}`);
        var to1 = encodeURIComponent(`${to}`);
        var query1 = 'searchCriteria[filterGroups][3][filters][3][field]=status&searchCriteria[filterGroups][3][filters][3][value]=' + status;
        var query2 = '&searchCriteria[filterGroups][0][filters][0][field]=created_at&searchCriteria[filterGroups][0][filters][0][value]=' + from1 + '&searchCriteria[filterGroups][0][filters][0][condition_type]=from&searchCriteria[filterGroups][1][filters][1][field]=created_at&searchCriteria[filterGroups][1][filters][1][value]=' + to1 + '&searchCriteria[filterGroups][1][filters][1][condition_type]=to';
        var query4 = '&searchCriteria[sortOrders][0][field]=entity_id&searchCriteria[sortOrders][1][direction]=DESC';
        var query3 = "&searchCriteria[currentPage]=" + page_number +"&searchCriteria[pageSize]=" + len;
        var actual_query = query1 + query2+ query3+ query4;
        console.log(actual_query);
        var endpointUrl = util.format('/orders?%s', actual_query);
        return restClient.get(endpointUrl);
    };

    // status wise order through API
    module.orderswithapi = function (from, to, status, len, page_number) {

        var actual_query = "";

        var query2 = "&searchCriteria[currentPage]=" + page_number +
            "&searchCriteria[pageSize]=" + len;


        if (from == "" && to == "") {
            if (status == "all") {
                actual_query = "searchCriteria[currentPage]=" + page_number +
                    "&searchCriteria[pageSize]=" + len;
            }
            else {
                var query = 'searchCriteria[filterGroups][0][filters][0][field]=status&searchCriteria[filterGroups][0][filters][0][value]=' + status;

                actual_query = query + query2;
            }

        }
        else {
            if (status == "all") {
                var query1 = 'searchCriteria[filterGroups][0][filters][0][field]=created_at&searchCriteria[filterGroups][0][filters][0][value]=' + from + '&searchCriteria[filterGroups][0][filters][0][condition_type]=from&searchCriteria[filterGroups][1][filters][1][field]=created_at&searchCriteria[filterGroups][1][filters][1][value]=' + to + '&searchCriteria[filterGroups][1][filters][1][condition_type]=to';

                actual_query = query1 + query2;
            }
            else {
                var query1 = 'searchCriteria[filterGroups][0][filters][0][field]=created_at&searchCriteria[filterGroups][0][filters][0][value]=' + from + '&searchCriteria[filterGroups][0][filters][0][condition_type]=from&searchCriteria[filterGroups][1][filters][1][field]=created_at&searchCriteria[filterGroups][1][filters][1][value]=' + to + '&searchCriteria[filterGroups][1][filters][1][condition_type]=to';

                var query = '&searchCriteria[filterGroups][0][filters][0][field]=status&searchCriteria[filterGroups][0][filters][0][value]=' + status;

                actual_query = query1 + query + query2;
            }

        }

        var endpointUrl = util.format('/orders?%s', actual_query);
        return restClient.get(endpointUrl);

    };

    module.pincodewiseorder = function (postcode) {

        var query = 'searchCriteria[filterGroups][0][filters][0][field]=postcode&searchCriteria[filterGroups][0][filters][0][value]=' + postcode;

        var endpointUrl = util.format('/orders?%s', query);
        return restClient.get(endpointUrl);
    };

    module.customerList = function () {
        var email = "anant@cashlu.com";


        // var query = 'searchCriteria=&searchCriteria[filterGroups][0][filters][0][addresses][0][postcode]=700126';
        // //'searchCriteria[filterGroups][0][filters][0][addresses][0][value]=' + encodeURIComponent(email);

        var query = 'searchCriteria=&searchCriteria[filterGroups][0][filters][0][field]=email&' +
            'searchCriteria[filterGroups][0][filters][0][value]=' + encodeURIComponent(email);

        var endpointUrl = util.format('/customers/search?%s', query);
        return restClient.get(endpointUrl);
    }

    module.customerListbymobile = function (mobile) {
        var phone = mobile;


        // var query = 'searchCriteria=&searchCriteria[filterGroups][0][filters][0][addresses][0][postcode]=700126';
        // //'searchCriteria[filterGroups][0][filters][0][addresses][0][value]=' + encodeURIComponent(email);

        var query = 'searchCriteria=&searchCriteria[filterGroups][0][filters][0][field]=customer_mobile&' +
            'searchCriteria[filterGroups][0][filters][0][value]=' + phone;
        //'searchCriteria[filterGroups][0][filters][0][condition_type]=eq';

        var endpointUrl = util.format('/customers/search?%s', query);
        return restClient.get(endpointUrl);
    }

    module.customerListbyemail = function (email) {
        // var phone = mobile;



        // var query = 'searchCriteria=&searchCriteria[filterGroups][0][filters][0][addresses][0][postcode]=700126';
        // //'searchCriteria[filterGroups][0][filters][0][addresses][0][value]=' + encodeURIComponent(email);

        var query = 'searchCriteria=&searchCriteria[filterGroups][0][filters][0][field]=email&' +
            'searchCriteria[filterGroups][0][filters][0][value]=' + email;
        //'searchCriteria[filterGroups][0][filters][0][condition_type]=eq';
        var endpointUrl = util.format('/customers/search?%s', query);
        // async function foo(){
        //     var response = restClient.get(endpointUrl);
        // };

        var p3 = new Promise(function (resolve, reject) {
            resolve(restClient.get(endpointUrl));
        });

        var x = Promise.all([p3])
            .then(function (res) {
                var x;
                for (var i = 0; i < res[0].items[0].custom_attributes.length; i++) {
                    x = res;
                    x[0][res[0].items[0].custom_attributes[i].attribute_code] = x[0].items[0].custom_attributes[i].value
                    // x[0].items[0][res[0].items[0].custom_attributes[i].attribute_code] = x[0].items[0].custom_attributes[i].attribute_code.value;


                }

                // console.log('x',JSON.stringify(x));
                return x;
                // console.log('Promise.all', res);
            })
            .catch(function (err) {
                console.error('Promise.all error', err);
            });

        return x;
        // foo().then(console.log(response));

        // return restClient.get(endpointUrl);
    }

    module.customerUpdate = function (userData) {
        // var userdata = { customer:
        //     { id:1,
        //         group_id: 1,
        //     email: '1111111112@cashlu.com',
        //       firstname: 'Anant',
        //       lastname: 'Jain3',
        //       store_id: 1,
        //       website_id: 1,
        //       addresses: [  ],
        //       disable_auto_group_change: 0,
        //       extension_attributes: { is_subscribed: false } } }
        var id = userData.customer.id;
        console.log(userData)

        return restClient.put(`/customers/${id}`, userData);
    }

    // module.adminToken = function () {

    //     var login = {
    //         "username": "admin",
    //         "password": "cashlu2018"
    //     }

    //     return restClient.post(`/integration/admin/token`, login);
    // }

    module.orderUpdate = function (userData) {


        return restClient.post(`/orders/`, userData);
    }

    module.statusUpdateWithComment = function (order_id, status) {
        return restClient.post(`/orders/${order_id}/comments`, status);
    }

    // order is cancel in magento to revert back the salable stock 
    module.cancelStateInMagento = function (order_id) {
        return restClient.post(`/orders/${order_id}/cancel`);
    }

    module.invoice = function (id) {
        return restClient.post(`/order/${id}/invoice/`)
    }

    module.itemWiseOrder = function () {
        var store_id = 1;


        var query = 'searchCriteria=&searchCriteria[filterGroups][0][filters][0][field]=store_id&' +
            'searchCriteria[filterGroups][0][filters][0][value]=' + store_id;

        var endpointUrl = util.format('/orders/items?%s', query);
        return restClient.get(endpointUrl);
    }

    // module.ItemsDetailsbyStatus = function (status) {
    //     var query = 'searchCriteria[filterGroups][0][filters][0][field]=status&searchCriteria[filterGroups][0][filters][0][value]=' + status;

    //     var endpointUrl = util.format('/orders/items?%s', query);
    //     return restClient.get(endpointUrl);
    // }

    // get count using status
    module.getCountStatusWise = function (status) {
        var query = 'searchCriteria[filterGroups][0][filters][0][field]=status&searchCriteria[filterGroups][0][filters][0][value]=' + status + '&searchCriteria[filterGroups][0][filters][0][condition_type]=eq&fields=total_count';
        var endpointUrl = util.format('/orders?%s', query);
        return restClient.get(endpointUrl);
    };

    module.getCountStatusWiseOrderList = function (order_id,status) {
        var query1 = 'searchCriteria[filterGroups][0][filters][0][field]=entity_id' +
        '&searchCriteria[filterGroups][0][filters][0][value]=' + order_id +
        '&searchCriteria[filterGroups][0][filters][0][condition_type]=in';

        var query2 = '&searchCriteria[filterGroups][1][filters][0][field]=status' +
        '&searchCriteria[filterGroups][1][filters][0][value]=' + status +
        '&searchCriteria[filterGroups][1][filters][0][condition_type]=in&fields=total_count,items[total_qty_ordered]';

        var query = query1+query2;
        var endpointUrl = util.format('/orders?%s', query);
        return restClient.get(endpointUrl);
    };

    module.billingAddressUpdate = function (parentid, address) {

        return restClient.put(`/orders/${parentid}`, address)
    }

    module.shipment = function (orderId, body) {
        console.log("body", body);
        // console.log("token", token)
        console.log("orderId", orderId);

        return restClient.post(`/order/${orderId}/ship`, body);
    }

    // stock
    module.productStock = function (productSku) {
        return restClient.get(`/stockItems/${productSku}`)
    }

    // stock
    module.getSalableStock = function (sku, stockId) {
        return restClient.get(`/inventory/get-product-salable-quantity/${sku}/${stockId}`)
    }

    // update product stock
    module.updateProductStock = function (body) {

        return restClient.post(`/inventory/source-items`, body);
    }

    // itembysku
    module.productbysku = function (productSku) {
        return restClient.get(`/products/${productSku}`)
    }

      // itembycategory
      module.productbycategory = function (productcategory) {
        return restClient.get(`/categories/${productcategory}`)
    }
    // product list using comma separated sku
    module.productbyskulist = function (sku) {
        var query = "searchCriteria[filter_groups][0][filters][0][field]=sku" +
            "&searchCriteria[filter_groups][0][filters][0][value]=" + sku +
            "&searchCriteria[filter_groups][0][filters][0][condition_type]=in";

        var endpointUrl = util.format('/products?%s', query);
        return restClient.get(endpointUrl);
    }



    // product list
    module.productbynameandsku = function (name, sku, page_number, len) {

        var actual_query = "";

        var query2 = "&searchCriteria[currentPage]=" + page_number +
            "&searchCriteria[pageSize]=" + len;

        if (name == "" && sku == "") {
            var actual_query = "searchCriteria[currentPage]=" + page_number +
                "&searchCriteria[pageSize]=" + len;
        }
        else {
            if (name != "" && sku != "") {
                var query = "searchCriteria[filter_groups][0][filters][0][field]=sku" +
                    "&searchCriteria[filter_groups][0][filters][0][value]=%25" + sku + "%25" +
                    "&searchCriteria[filter_groups][0][filters][0][condition_type]=like" +
                    "&searchCriteria[filter_groups][1][filters][0][field]=name" +
                    "&searchCriteria[filter_groups][1][filters][0][value]=%25" + name + "%25" +
                    "&searchCriteria[filter_groups][1][filters][0][condition_type]=like";

                actual_query = query + query2;
            }
            else if (name != "") {
                var query = "searchCriteria[filter_groups][1][filters][0][field]=name" +
                    "&searchCriteria[filter_groups][1][filters][0][value]=%25" + name + "%25" +
                    "&searchCriteria[filter_groups][1][filters][0][condition_type]=like";

                actual_query = query + query2;

            }
            else if (sku != "") {
                var query = "searchCriteria[filter_groups][0][filters][0][field]=sku" +
                    "&searchCriteria[filter_groups][0][filters][0][value]=%25" + sku + "%25" +
                    "&searchCriteria[filter_groups][0][filters][0][condition_type]=like";

                actual_query = query + query2;

            }
        }



        var query1 = "searchCriteria[filter_groups][0][filters][0][field]=updated_at" +
            "&searchCriteria[filter_groups][0][filters][0][value]=2019-01-15" +
            "&searchCriteria[filter_groups][0][filters][0][condition_type]=lteq";


        var endpointUrl = util.format('/products?%s', actual_query);
        return restClient.get(endpointUrl);
    }

    // getLatestProduct
    module.getLatestProduct = function (page_number, len) {

        var actual_query = "";

        var query2 = "searchCriteria[currentPage]=" + page_number +
            "&searchCriteria[pageSize]=" + len;

        var query = "&searchCriteria[sortOrders][0][field]=created_at" +
            "&searchCriteria[sortOrders][1][direction]=DESC"

        actual_query = query2 + query;
        var endpointUrl = util.format('/products?%s', actual_query);
        return restClient.get(endpointUrl);
    }

    // update Product
    module.updateproduct = function (body) {
        return restClient.post(`/products`, body);
    }

    module.getdetailsbyorderId_status = function(order_id,status){
        var query1 = 'searchCriteria[filterGroups][0][filters][0][field]=entity_id' +
            '&searchCriteria[filterGroups][0][filters][0][value]=' + order_id +
            '&searchCriteria[filterGroups][0][filters][0][condition_type]=in';

            var query2 = '&searchCriteria[filterGroups][1][filters][0][field]=status' +
            '&searchCriteria[filterGroups][1][filters][0][value]=' + status +
            '&searchCriteria[filterGroups][1][filters][0][condition_type]=in';

            var query = query1+query2

        var endpointUrl = util.format('/orders?%s', query);
        return restClient.get(endpointUrl);
    }
    // product list with dynamic field
    module.productbyDynamicField = function (field,value) {
        var query = "searchCriteria[filter_groups][0][filters][0][field]=" + field+
            "&searchCriteria[filter_groups][0][filters][0][value]=" + value +
            "&searchCriteria[filter_groups][0][filters][0][condition_type]=eq";

        var endpointUrl = util.format('/products?%s', query);
        return restClient.get(endpointUrl);
    }

    return module;
}
